function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

function createTable() {
    var existingTables = document.getElementsByTagName('table');
    for(table of existingTables){
        table.remove();
    }

    const countRows = document.getElementById('rows').value;
    const countCols = document.getElementById('cols').value

    if(isNaN(parseInt(countRows)) || countRows < 1)
    {
        throw 'Invalid Row Input';
    }
    if(isNaN(parseInt(countCols)) || countCols < 1)
    {
        throw 'Invalid Column Input'
    }

    const currentTable = document.createElement('table');
    for(var i = 0; i < document.getElementById('rows').value; i++){
        const newRow = document.createElement('tr');
        newRow.className = "row";
        for(var j = 0; j < document.getElementById('cols').value; j++){
            const newCol = document.createElement('td');
            newCol.className = "cell";
            newCol.innerHTML = (i + 1).toString().concat((j + 1).toString());
            newRow.appendChild(newCol);
        }
        currentTable.className = "current_table";
        currentTable.appendChild(newRow);
    }
    document.body.appendChild(currentTable);

    const defaultCellColor = currentTable.getElementsByClassName('cell')[0].style.backgroundColor;
    currentTable.addEventListener('click', function(e){
        const target = e.target;
        if(target.matches('td'))
        {
            if(target.style.backgroundColor == defaultCellColor)
            {
                target.style.backgroundColor = getRandomColor();
            }
            else
            {
                target.style.backgroundColor = defaultCellColor;
            }
        }
    })
}

var element = document.getElementById('create_table');
element.addEventListener('click', createTable);